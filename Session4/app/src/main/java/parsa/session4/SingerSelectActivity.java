package parsa.session4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ExpandedMenuView;
import android.view.View;
import android.webkit.WebView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import parsa.session4.Adapter.SingerListAdapter;
import parsa.session4.Models.MyBrowser;
import parsa.session4.Models.SingerModel;

public class SingerSelectActivity extends AppCompatActivity {

    ListView SingersList;

    TextView WelcomeText;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singer_select);

        Intent intent = getIntent();

        webView = (WebView) findViewById(R.id.webView);
        webView.setVisibility(View.GONE);

        String UserID = intent.getStringExtra("UserName");

        WelcomeText = (TextView) findViewById(R.id.WelcomeText);

        if (Hawk.get("UserName") != "") {
            UserID = Hawk.get("UserName");
        }

        WelcomeText.setText("Welcome dear " + UserID + "!"
                + "\nPlease Choose your favorite Singer from the list");

        SingersList = (ListView) findViewById(R.id.SingersList);

        SingerModel Singer1 = new SingerModel("Mohsen Chavoshi", R.drawable.mohsenchavoshi, "https://fa.wikipedia.org/wiki/%D9%85%D8%AD%D8%B3%D9%86_%DA%86%D8%A7%D9%88%D8%B4%DB%8C", 1);
        SingerModel Singer2 = new SingerModel("Mohsen Yeganeh", R.drawable.mohsenyeganeh, "https://fa.wikipedia.org/wiki/%D9%85%D8%AD%D8%B3%D9%86_%DB%8C%DA%AF%D8%A7%D9%86%D9%87", 2);
        SingerModel Singer3 = new SingerModel("Mohsen Namjoo", R.drawable.mohsennamjoo, "https://fa.wikipedia.org/wiki/%D9%85%D8%AD%D8%B3%D9%86_%D9%86%D8%A7%D9%85%D8%AC%D9%88", 3);
        SingerModel Singer4 = new SingerModel("Farzad Farzin", R.drawable.farzadfarzin, "https://fa.wikipedia.org/wiki/%D9%81%D8%B1%D8%B2%D8%A7%D8%AF_%D9%81%D8%B1%D8%B2%DB%8C%D9%86", 4);
        SingerModel Singer5 = new SingerModel("The Pallet", R.drawable.thepallet, "https://fa.wikipedia.org/wiki/%DA%AF%D8%B1%D9%88%D9%87_%D9%BE%D8%A7%D9%84%D8%AA", 5);
        SingerModel Singer6 = new SingerModel("The Chartar", R.drawable.thechartar, "https://fa.wikipedia.org/wiki/%DA%86%D8%A7%D8%B1%D8%AA%D8%A7%D8%B1", 6);
        SingerModel Singer7 = new SingerModel("Ehsan Khajeh Amiri", R.drawable.ehsankhajeamiri, "https://fa.wikipedia.org/wiki/%D8%A7%D8%AD%D8%B3%D8%A7%D9%86_%D8%AE%D9%88%D8%A7%D8%AC%D9%87%E2%80%8C%D8%A7%D9%85%DB%8C%D8%B1%DB%8C", 7);
        SingerModel Singer8 = new SingerModel("Xaniar Khosravi", R.drawable.xaniarkhosravi, "https://fa.wikipedia.org/wiki/%D8%B2%D8%A7%D9%86%DB%8C%D8%A7%D8%B1_%D8%AE%D8%B3%D8%B1%D9%88%DB%8C", 8);
        SingerModel Singer9 = new SingerModel("Sirvan Khosravi", R.drawable.sirvankhosravi, "https://fa.wikipedia.org/wiki/%D8%B3%DB%8C%D8%B1%D9%88%D8%A7%D9%86_%D8%AE%D8%B3%D8%B1%D9%88%DB%8C", 9);

        List<SingerModel> Singers = new ArrayList<>();
        Singers.add(Singer1);
        Singers.add(Singer2);
        Singers.add(Singer3);
        Singers.add(Singer4);
        Singers.add(Singer5);
        Singers.add(Singer6);
        Singers.add(Singer7);
        Singers.add(Singer8);
        Singers.add(Singer9);

        SingerListAdapter SingerAdapter = new SingerListAdapter(this, Singers);
        SingersList.setAdapter(SingerAdapter);

        SingersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SingerModel SingerChosen = (SingerModel) parent.getItemAtPosition(position);
                webView.setWebViewClient(new MyBrowser());
                webView.getSettings().setLoadsImagesAutomatically(true);
                webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl(SingerChosen.getWebsiteURL());
                webView.setVisibility(View.VISIBLE);
            }
        });

    }
}
