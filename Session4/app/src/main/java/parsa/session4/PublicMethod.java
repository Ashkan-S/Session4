package parsa.session4;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Created by Ashkan on 6/13/2017.
 */

public class PublicMethod {

    Context mContext;

    public PublicMethod(Context mContext) {
        this.mContext = mContext;
    }

    public void ShowToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public void SavePref(String KeyPref, String ValuePref) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                .putString(KeyPref, ValuePref).commit();
    }

    public String ShowPref(String KeyPref, String DefaultValuePref) {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString(KeyPref, DefaultValuePref);

    }
}
