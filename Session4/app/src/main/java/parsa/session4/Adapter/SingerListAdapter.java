package parsa.session4.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import parsa.session4.Models.SingerModel;
import parsa.session4.R;

/**
 * Created by Ashkan on 6/13/2017.
 */

public class SingerListAdapter extends BaseAdapter {

    Context mContext;
    List<SingerModel> singers;

    public SingerListAdapter(Context mContext, List<SingerModel> singers) {
        this.mContext = mContext;
        this.singers = singers;
    }

    @Override
    public int getCount() {
        return singers.size();
    }

    @Override
    public Object getItem(int position) {
        return singers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.singer_list_item, parent, false);

        ImageView SingerPic = (ImageView) rowView.findViewById(R.id.SingerPic);
        TextView SingerName = (TextView) rowView.findViewById(R.id.SingerName);

        SingerName.setText(singers.get(position).getSingerName());

        //Drawable Picture = ResourcesCompat.getDrawable(mContext.getResources(), singers.get(position).getSingerPic(), null);
        //Drawable Picture = mContext.getResources().getDrawable(singers.get(position).getSingerPic());
        Drawable Picture = ContextCompat.getDrawable(mContext, singers.get(position).getSingerPic());
        SingerPic.setImageDrawable(Picture);

        return rowView;
    }
}
