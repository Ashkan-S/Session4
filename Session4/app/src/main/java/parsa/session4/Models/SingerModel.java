package parsa.session4.Models;

/**
 * Created by Ashkan on 6/13/2017.
 */

public class SingerModel {
    String SingerName;
    int SingerPic;
    String WebsiteURL;
    int id;

    public SingerModel(String singerName, int singerPic, String websiteURL, int id) {
        SingerName = singerName;
        SingerPic = singerPic;
        WebsiteURL = websiteURL;
        this.id = id;
    }

    public String getSingerName() {
        return SingerName;
    }

    public void setSingerName(String singerName) {
        SingerName = singerName;
    }

    public int getSingerPic() {
        return SingerPic;
    }

    public void setSingerPic(int singerPic) {
        SingerPic = singerPic;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
