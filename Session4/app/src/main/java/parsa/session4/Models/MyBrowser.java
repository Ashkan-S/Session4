package parsa.session4.Models;

import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Ashkan on 6/14/2017.
 */

public class MyBrowser extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        {
            view.loadUrl(request.toString());
            return true;
        }
    }
}
