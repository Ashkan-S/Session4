package parsa.session4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

public class GetUserNameActivity extends AppCompatActivity {
    EditText UserName;
    EditText Password;
    Button Next;
    CheckBox RememberMe;
    //PublicMethod PublicMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_user_name);

        Next = (Button) findViewById(R.id.Next);
        UserName = (EditText) findViewById(R.id.UserName);
        Password = (EditText) findViewById(R.id.Password);
        RememberMe = (CheckBox) findViewById(R.id.RememberMe);
        //PublicMethod = new PublicMethod(GetUserNameActivity.this);

        Hawk.init(GetUserNameActivity.this).build();

        if (Hawk.get("UserName") != "") {
            UserName.setText(Hawk.get("UserName", ""));
            Password.setText(Hawk.get("Password", ""));
        }
        //if (PublicMethod.ShowPref("UserName", "") != "") {
        //   UserName.setText(PublicMethod.ShowPref("UserName", ""));
        //    Password.setText(PublicMethod.ShowPref("Password", ""));
        //}
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetUserNameActivity.this, SingerSelectActivity.class);
                if (RememberMe.isChecked()) {
                    Hawk.put("UserName", UserName.getText().toString());
                    Hawk.put("Password", Password.getText().toString());

                    //PublicMethod.SavePref("UserName", UserName.getText().toString());
                    //PublicMethod.SavePref("Password", Password.getText().toString());
                    //PublicMethod.ShowToast("True");
                } else {
                    Hawk.put("UserName", "");
                    Hawk.put("Password", "");

                    intent.putExtra("UserName", UserName.getText().toString());
                    //PublicMethod.SavePref("UserName", "");
                    //PublicMethod.SavePref("Password", "");
                    //PublicMethod.ShowToast("False");
                }
                startActivity(intent);
                finish();
            }
        });
    }
}